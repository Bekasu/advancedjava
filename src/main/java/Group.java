public class Group {
    private int groupId;
    private String groupName;

    public Group(){}

    public Group(int id, String name){
        this.groupId = id;
        this.groupName = name;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "Group = " + groupName;
    }
}
