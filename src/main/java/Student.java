public class Student {
    private int id;
    private String fname;
    private String lname;
    private int groupId;

    public Student(){}

    public Student(int id, String fname, String lname, int groupId){
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.groupId = groupId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", fname='" + fname +
                ", lname='" + lname +
                ", groupId=" + groupId +
                '}';
    }
}
