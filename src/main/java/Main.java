import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5433/postgres";
        String user = "postgres";
        String pass = "123456";
        Connection connection = null;
        Statement s1 = null;
        Statement s2 = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, pass);
            if (connection != null) {
                System.out.println("connected");

                s1 = connection.createStatement();
                s2 = connection.createStatement();

                ResultSet rs = s1.executeQuery("SELECT * FROM students;");
                while (rs.next()){
                    Student student = new Student(rs.getInt("student_id"),
                                                rs.getString("student_fname"),
                                                rs.getString("student_lname"),
                                                rs.getInt("student_group_id"));

                    ResultSet rs2 = s2.executeQuery("SELECT * FROM student_groups WHERE group_id = " + student.getGroupId() + ";");
                    Group group = new Group();

                    while (rs2.next()){
                        group.setGroupId(rs2.getInt("group_id"));
                        group.setGroupName(rs.getString("group_name"));
                    }
                    System.out.print(student);
                    System.out.println(group);
                }

            } else {
                System.out.println("failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }









    }
}
