CREATE TABLE student_groups(
	group_id int PRIMARY KEY,
	group_name varchar(255)
);

CREATE TABLE students(
	student_id int PRIMARY KEY,
	student_fname varchar(255),
	student_lname varchar(255),
	student_group_id int,
	CONSTRAINT foreign_key_cons FOREIGN KEY(student_group_id) REFERENCES student_groups(group_id)
);

INSERT INTO student_groups(group_id,group_name)
VALUES (1,'SE-1901'),
		(2,'CT-1901'),
		(3,'MT-1901'),
		(4,'CS-1902'),
		(5,'BD-1901');

INSERT INTO students(student_id,student_fname,student_lname,student_group_id)
VALUES (1,'Bekdaulet','S',4),
		(2,'Almaz','N',4),
		(3,'Yernur','B',1),
		(4,'Kuanysh','A',1),
		(5,'Didar','Z',5),
		(6,'Angsar','M',5),
		(7,'Yergazi','A',3),
		(8,'Arman','S',3),
		(9,'Dana','M',2),
		(10,'Dias','A',2);

SELECT * FROM student_groups;
SELECT * FROM students;